<?php


use src\Bitm\SEIP106607\Gender;
include_once ('../../../'.'vendor/autoload.php');
$class = new Gender();
$gender = $class->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Gender</title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <td>SL.No.</td>
                    <td>Name</td>
                    <td>Gender</td>
                    <td colspan="3" style="text-align: center">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($gender as $info){
                    $i++;
                
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $info['name'];?></td>
                    <td><?php echo $info['gender'];?></td>
                    <td>
                        <form action="#" method="post">
                            <input type="hidden" name="id"/>
                            <input type="submit" name="id" value="view"/>
                        </form>
                    </td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                            <input type="submit" value="Edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                            <input type="submit" value="Delete"/>
                        </form>
                       
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div>
            <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a></br> 
        
        </div>
    </body>
</html>