<?php


use src\Bitm\SEIP106607\Gender;
include_once ('../../../'.'vendor/autoload.php');
$Editing=new Gender();
$ID = $_POST['id'];
$update = $Editing->edit($ID);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create gender</title>
    </head>
    <body>
        <form action="store.php" method="post">
            <h1>Give the Information</h1>
            <fieldset>
                <legend>Edit the form</legend>
                <label>Name</label>
                <input type="text" name="name" value="<?php echo $update['name'];?>"/>
                <input type="hidden" name="id" value="<?php echo $update['id'];?>"/></br></br>
                
                <label>Gender : </label>
                <input type="radio" name="gender"  value="Male" <?php if(isset($update['gender']) && $update['gender']=='Male')echo "checked"?>/>Male
                <input type="radio" name="gender" value="Female" <?php if(isset($update['gender']) && $update['gender']=='Female')echo "checked"?>/>Female</br></br>
                <input type="submit" value="Save"/>
            </fieldset>
        </form>
    </body>
</html>