<?php
use src\Bitm\SEIP106607\Book;
include_once ('../../../'.'vendor/autoload.php');

$book = new Book();
$books = $book->index();
$trs = "";
?>
<?php 
                $i = 0;
                foreach ($books as $book){
                    $i++;
                    
                    $trs .="<tr>";
                    $trs .="<td>".$i."</td>";
                    $trs .="<td>".$book['title']."</td>";
                    $trs .="<td>".$book['author']."</td>";
                    $trs .="<tr>";
                 }
            ?>
<?php
$html = <<<IMON
<!DOCTYPE html>
<html>
    <head>
        <title>Book list</title>
    </head>
    <body>
       
        <table border ='1'>
            <thead>
                <tr>
                    <td>SL. NO.</td>
                    <td>Title</td>
                    <td>Author</td>
                    
                </tr>
            </thead>
            <tbody>
                
               echo $trs;
            </tbody>
            
        </table>
        
    </body>
</html>
IMON;
?>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'final_for_atomic_project'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';


$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;