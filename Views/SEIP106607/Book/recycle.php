<?php
use src\Bitm\SEIP106607\Book;
include_once ('../../../'.'vendor/autoload.php');

$book = new Book();
$books = $book->recycle();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Recycle Bin</title>
    </head>
    <body>
        <table border ='1'>
            <thead>
                <tr>
                    <td>SL. NO.</td>
                    <td>Title</td>
                    <td>Author</td>
                    <td colspan="2" style="text-align: center;">Action</td>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $i=0;
                    foreach($books as $list){
                              $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $list['title'];?></td>
                    <td><?php echo $list['author'];?></td>
                    <td>
                        <form action="recover.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="Recover"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="Parmanently Delete"/>
                        </form>
                    </td>
                    
                </tr>
                <?php } ?>
            </tbody>
            
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> 
        <a href="index.php"><input type="submit" name="create" value="Book Library"></a>
        </div>
    </body>
</html>