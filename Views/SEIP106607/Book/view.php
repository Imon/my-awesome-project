<?php
use src\Bitm\SEIP106607\Book;
include_once ('../../../'.'vendor/autoload.php');
$id = $_POST['id'];

$book = new Book();
$books = $book->view($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Recycle Bin</title>
    </head>
    <body>
        <table border ='1'>
            <thead>
                <tr>
                    <td>SL. NO.</td>
                    <td>Title</td>
                    <td>Author</td>
                    <td colspan="3" style="text-align: center;">Action</td>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $i=1;
                 
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $books['title'];?></td>
                    <td><?php echo $books['author'];?></td>
                    
                  <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $books['id'];?>"/>
                            <input type="submit" value="Edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $books['id'];?>"/>
                            <input type="submit" value="Delete"/>
                        </form>
                    </td>
                    <td>
                        <form action="trash.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $books['id'];?>"/>
                            <input type="submit" value="Trash"/>
                        </form>
                    </td>
                    
                </tr>
               
            </tbody>
            
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> 
        <a href="index.php"><input type="submit" name="create" value="Book Library"></a>
        </div>
    </body>
</html>
