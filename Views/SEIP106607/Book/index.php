<?php
use src\Bitm\SEIP106607\Book;
include_once ('../../../'.'vendor/autoload.php');

$book = new Book();
$books = $book->index();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Book list</title>
    </head>
    <body>
        <div>
            <span>Download as <a href="pdf.php">PDF</a> | <a href="excel.php">Excel</a>
            Or <a href="mail.php">Email to Friend</a></span>
        </div>
        <table border ='1'>
            <thead>
                <tr>
                    <td><b>SL. NO.</b></td>
                    <td><b>Title</b></td>
                    <td><b>Author</b></td>
                    <td colspan="4" style="text-align: center;"><b>Action</b></td>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $i=0;
                    foreach($books as $list){
                              $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $list['title'];?></td>
                    <td><?php echo $list['author'];?></td>
                    <td>
                        <form action="view.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="View"/>
                        </form>
                    </td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="delete"/>
                        </form>
                    </td>
                    <td>
                        <form action="trash.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="Trash"/>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a> 
        <a href="recycle.php"><input type="submit" name="create" value="Recycle Bin"></a>
        </div>
    </body>
</html>