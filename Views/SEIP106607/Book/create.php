
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css">
    </head>
    <body style="background-color: gray">
        <form action="store.php" method="post">
            <div class="form-group">
                <div class="col-md-4 col-md-offset-4">
                    <label>Book Title</label>
                
                <input type="text" name="title" class="form-control" placeholder="Write Your Book Title">
                 
                </div>
            </div>
                <div class="form-group">
                <div class="col-md-4 col-md-offset-4">
                    <label>Author Name</label>
                
                <input type="text" name="author" class="form-control" placeholder="Write The Author Name">
                 
                </div>
                </div>
                <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-default">Save</button>
                </div>
                </div>
                
            
        </form>
    </body>
</html>